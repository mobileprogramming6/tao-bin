import 'dart:io';

int price = 0;
String dt = '', chooseMenu = '', chooseDrinkType = '';

void main() {
  showWelcome();

  String drinkCategory = selectDrinkCategory();

  if (drinkCategory == '1') {                 // Coffee
    String menu = menuOfCoffee(drinkCategory);
    showSeparator();
    groupShow(menu, drinkCategory);
  } else if (drinkCategory == '2') {          // Tea
    String menu = menuOfTea(drinkCategory);
    showSeparator();
    groupShow(menu, drinkCategory);
  } else if (drinkCategory == '3') {          // Milk
    String menu = menuOfMilkChocoCaramel(drinkCategory);
    showSeparator();
    groupShow(menu, drinkCategory);
  } else if (drinkCategory == '4') {          // Protein
    String menu = menuOfProteinShake(drinkCategory);
    showSeparator();
    groupShow(menu, drinkCategory);
  } else if (drinkCategory == '5') {          // Soda
    String menu = menuOfSoda(drinkCategory);
    showSeparator();
    groupShow(menu, drinkCategory);
  } else {
    print('Error: Please input again.');
    showSeparator();
  }
  
}

void groupShow(String menu, String drinkCategory) {

  if (drinkCategory=='5' && menu!='1') {
    String soda = addSoda();
    showSeparator();
    String sweetnessLevel = selectSweetnessLevel();
    showSeparator();   
    String sm = showChooseMenu(drinkCategory, menu);
    String sdt = showDrinkType(dt);
    int price = calculatePrice(drinkCategory, menu);
    print('You choose $sdt $sm');
    print('Input Soda : ${showAddSoda(soda)}');
    print('Level of Sweetness : ${showSweetnessLevel(sweetnessLevel)}');
    print('Total Price = ${totalPrice(dt, price)}');
    showSeparator();
  } else {
    String dt = selectDrinkType(drinkCategory, menu);    
    showSeparator();
    String sweetnessLevel = selectSweetnessLevel();
    showSeparator();   
    String sm = showChooseMenu(drinkCategory, menu);
    String sdt = showDrinkType(dt);
    int price = calculatePrice(drinkCategory, menu);
    print('You choose $sdt $sm');
    print('Level of Sweetness : ${showSweetnessLevel(sweetnessLevel)}');
    print('Total Price = ${totalPrice(dt, price)}');
    showSeparator();
  }

  int totalprice = totalPrice(dt, price);

  print('PAY BY CASH');
  print('Please enter your amount (Baht) : ');
  int amount = int.parse(stdin.readLineSync()!);
  showSeparator();

  print('Your change is ${payment(totalprice,amount)} Baht.');
  showSeparator();

  print('Thank you for using Tao-Bin');
  print('Please wait a moment for your beverage ...');
  print(' :) ');
  showSeparator();
}

int payment(int totalprice, int amount) {
  return amount - totalprice;
}

String addSoda() {
  print('Do you want to put soda ? (Yes(y)/No(n)) (alphabet)');
  String soda = stdin.readLineSync() as String;
  return soda;
}

String showAddSoda(String soda) {
  if (soda == 'y') {
    soda = 'Yes';
  } else {
    soda = 'No';
  }
  return soda;
}

String selectSweetnessLevel() {
  print('1.No Sugar');
  print('2.Less Sweet');
  print('3.Just Right');
  print('4.Sweet');
  print('5.Very Sweet');
  print('Please Select Sweetness Level(number): ');
  String sweetnessLevel = stdin.readLineSync() as String;
  return sweetnessLevel;
}

String showSweetnessLevel(String sweetnessLevel) {
  switch (sweetnessLevel) {
    case '1':   sweetnessLevel = 'No Sugar';      break;
    case '2':   sweetnessLevel = 'Less Sweet';    break;
    case '3':   sweetnessLevel = 'Just Right';    break;
    case '4':   sweetnessLevel = 'Sweet';         break;
    case '5':   sweetnessLevel = 'Very Sweet';    break;
    default:  sweetnessLevel = 'Just Right';  break;
  }
  return sweetnessLevel;
}

int totalPrice(String dt, int price) {
  if (dt == 'i') {
    price += 5;
  } else if (dt == 's') {
    price += 15;
  }
  return price;
}

int calculatePrice(String drinkCategory, String menu) {
  if (drinkCategory == '1') {                 // drinkCategory -- Coffee
    switch (menu) {
      case '1':
        price = 25;
        break;

      case '2':
      case '9':
        price = 30;
        break;

      case '7':
        price = 45;
        break;

      case '3':
      case '4':
        price = 35;
        break;

      default: price = 40; break;
    }
  } else if (drinkCategory == '2') {          // drinkCategory -- Tea
    switch (menu) {
      case '2':                               // Thai Milk Tea
      case '3':                               // Taiwanese Tea
        price = 35;
        break;

      case '4':                               // Matcha Latte
        price = 40;
        break;

      case '7':                               // Strawberry tea
        price = 30;
        break;

      default: price = 20; break;
    }
  } else if (drinkCategory == '3') {          // drinkCategory -- Milk
    switch (menu) {
      case '2':                               // Cocoa
        price = 30;
        break;

      case '5':                               // Milk
        price = 25;
        break;

      default: price = 35; break;
    }
  } else if (drinkCategory == '4') {          // drinkCategory -- Protein Shake
    switch (menu) {
      case '9':                               // Plain protein shake
      case '10':                              // milk shake
        price = 55;
        break;

      default: price = 60; break;
    }
  } else if (drinkCategory == '5') {          // drinkCategory -- Soda
    switch (menu) {
      case '1':                               // Pepsi
        price = 15;
        break;

      case '2':                               // Lychee
      case '7':                               // Sala
        price = 20;
        break;

      case '4':                               // Strawberry
        price = 30;
        break;

      default: price = 30; break;
    }
  }

  return price;

}

String selectDrinkType(String drinkCategory, String menu) {
  if (drinkCategory == '1') {                 // Coffee
    switch (menu) {
      case '1': 
        {
          hot();
        } 
        break;

      case '3':
      case '4':
      case '5':
        {
          hot();
          iced();
          smoothies();
        }
        break;

      default:
        {
          hot(); 
          iced();
        } 
        break;
    }
  }

  if (drinkCategory == '2') {                 // Tea
    switch (menu) {
      case '2':
      case '4': 
        {
          hot(); 
          iced(); 
          smoothies();
        } 
        break;

      default:
        {
          hot(); 
          iced();
        } 
        break;
    }
  }

  if (drinkCategory == '3') {                 // Milk
    switch (menu) {
      case '2':
      case '5':
        {
          hot();
          iced();
          smoothies();
        }
        break;

      case '6':
        {
          iced();
          smoothies();
        }
        break;

      default:
        {
          hot();
          iced();
        }
        break;
    }   
  }

  if (drinkCategory == '4') {                 // Protein Shake
    iced();
    smoothies();
  }

  if (drinkCategory == '5') {                 // Soda
    if (menu == '1'){
      iced();
      smoothies();
    }
  }
  
  print('Please Select Drink Type an alphabet:');
  String dt = stdin.readLineSync() as String;  
  return dt;
}

void hot() {
  print('--------- Hot(h) ---------');
}

void iced() {
  print('------ Iced(i)(+5฿) ------');
}

void smoothies() {
  print('--- Smoothies(s)(+15฿) ---');
}

String showDrinkType(String dt) {
  if (dt == 'h') {                            // Hot
    chooseDrinkType = 'Hot';
  } else if (dt == 'i') {                     // Iced
    chooseDrinkType = 'Iced';
  } else if (dt == 's') {                     // Smoothies
    chooseDrinkType = 'Smoothies';
  }
  return chooseDrinkType;
}

String showChooseMenu(String drinkCategory, String menu) {
  if (drinkCategory == '1') {                 // drinkCategory -- Coffee
    if (menu == '1') {                        // Espresso
      chooseMenu = 'Espresso';
    } else if (menu == '2') {                 // Americano
      chooseMenu = 'Americano';
    } else if (menu == '3') {                 // Latte
      chooseMenu = 'Latte';
    } else if (menu == '4') {                 // Cappuccino
      chooseMenu = 'Cappuccino';
    } else if (menu == '5') {                 // Mocha
      chooseMenu = 'Mocha';
    } else if (menu == '6') {                 // Caramel Latte
      chooseMenu = 'Caramel Café Latte';
    } else if (menu == '7') {                 // Matcha Latte
      chooseMenu = 'Matcha Café Latte';
    } else if (menu == '8') {                 // Thai Tea Café Latte
      chooseMenu = 'Thai Tea Café Latte';
    } else if (menu == '9') {                // Lychee Americano
      chooseMenu = 'Lychee Americano';
    }
  } else if (drinkCategory == '2') {          // drinkCategory -- Tea
    if (menu == '1') {                        // Chrysanthemum Tea
      chooseMenu = 'Chrysanthemum Tea';
    } else if (menu == '2') {                 // Thai Milk Tea
      chooseMenu = 'Thai Milk Tea';
    } else if (menu == '3') {                 // Taiwanese Tea
      chooseMenu = 'Taiwanese Tea';
    } else if (menu == '4') {                 // Matcha Green Tea
      chooseMenu = 'Matcha Latte';
    } else if (menu == '5') {                 // Limenade Tea
      chooseMenu = 'Limenade Tea';
    } else if (menu == '6') {                 // Lychee Tea
      chooseMenu = 'Lychee Tea';
    } else if (menu == '7') {                 // Strawberry Tea
      chooseMenu = 'Strawberry Tea';
    } 
  } else if (drinkCategory == '3') {          // drinkCategory -- Milk
    if (menu == '1') {                        // Caramel Milk
      chooseMenu = 'Caramel Milk';
    } else if (menu == '2') {                 // Cocoa
      chooseMenu = 'Cocoa';
    } else if (menu == '3') {                 // Caramel Cocoa
      chooseMenu = 'Caramel Cocoa';
    } else if (menu == '4') {                 // Brown Sugar Milk
      chooseMenu = 'Brown Sugar Milk';
    } else if (menu == '5') {                 // Milk
      chooseMenu = 'Milk';
    } else if (menu == '6') {                 // Pink milk
      chooseMenu = 'Pink milk';
    }
  } else if (drinkCategory == '4') {          // drinkCategory -- Protein Shake
    if (menu == '1') {                        // Matcha Protein Shake
      chooseMenu = 'Matcha Protein Shake';
    } else if (menu == '2') {                 // Chocolate Protein Shake
      chooseMenu = 'Chocolate Protein Shake';
    } else if (menu == '3') {                 // Strawberry Protein Shake
      chooseMenu = 'Strawberry Protein Shake';
    } else if (menu == '4') {                 // Espresso Protein Shake
      chooseMenu = 'Espresso Protein Shake';
    } else if (menu == '5') {                 // Thai Tea Protein Shake
      chooseMenu = 'Thai Tea Protein Shake';
    } else if (menu == '6') {                 // Brown Sugar Protein Shake
      chooseMenu = 'Brown Sugar Protein Shake';
    } else if (menu == '7') {                 // Taiwanese Tea Protein Shake
      chooseMenu = 'Taiwanese Tea Protein Shake';
    } else if (menu == '8') {                 // Caramel Protein Shake
      chooseMenu = 'Caramel Protein Shake';
    } else if (menu == '9') {                 // Plain Protein Shake
      chooseMenu = 'Plain Protein Shake';
    } else if (menu == '10') {                // Milk Shake
      chooseMenu = 'Milk Shake';
    }
  } else if (drinkCategory == '5') {          // drinkCategory -- Soda
    if (menu == '1') {                        // Pepsi
      chooseMenu = 'Pepsi';
    } else if (menu == '2') {                 // Limenade Soda
      chooseMenu = 'Iced Limenade';
    } else if (menu == '3') {                 // Lychee Soda
      chooseMenu = 'Iced Lychee';
    } else if (menu == '4') {                 // Strawberry Soda
      chooseMenu = 'Iced Strawberry';
    } else if (menu == '5') {                 // Plum Soda
      chooseMenu = 'Iced Plum';
    } else if (menu == '6') {                 // Ginger Soda
      chooseMenu = 'Iced Ginger Soda';
    }  else if (menu == '7') {                 // Sala Soda
      chooseMenu = 'Iced Sala';
    } else if (menu == '8') {                 // Limenade Sala Soda
      chooseMenu = 'Iced Limenade Sala';
    }
  }

  return chooseMenu;

}

String menuOfSoda(String drinkCategory) {
  print('Menu of Soda & Other :');
  print('1.Pepsi                          15฿');
  print('2.Iced Limenade (Soda)           20฿');
  print('3.Iced Lychee (Soda)             25฿');
  print('4.Iced Strawberry (Soda)         30฿');
  print('5.Iced Plum (Soda)               25฿');
  print('6.Iced Ginger Soda               25฿');
  print('7.Iced Sala (Soda)               20฿');
  print('8.Iced Limenade Sala (Soda)      25฿');
  print('Please Select Menu of Soda or Other(number): ');
  String menu = stdin.readLineSync() as String;
  return menu;
}

String menuOfProteinShake(String drinkCategory) {
  print('Menu of Protein Shake :');
  print('1.Matcha Protein Shake           60฿');
  print('2.Chocolate Protein Shake        60฿');
  print('3.Strawberry Protein Shake       60฿');
  print('4.Espresso Protein Shake         60฿');
  print('5.Thai Tea Protein Shake         60฿');
  print('6.Brown Sugar Protein Shake      60฿');
  print('7.Taiwanese Tea Protein Shake    60฿');
  print('8.Caramel Protein Shake          60฿');
  print('9.Plain Protein Shake            55฿');
  print('10.Milk Shake                    55฿');
  print('Please Select Menu of Protein Shake(number): ');
  String menu = stdin.readLineSync() as String;
  return menu;
}

String menuOfMilkChocoCaramel(String drinkCategory) {
  print('Menu of Milk, Choco & Caramel :');
  print('1.Caramel Milk                   35฿');
  print('2.Cocoa                          30฿');
  print('3.Caramel Cocoa                  35฿');
  print('4.Brown Sugar Milk               35฿');
  print('5.Milk                           25฿');
  print('6.Pink milk                      35฿');
  print('Please Select Menu of Milk(number): ');
  String menu = stdin.readLineSync() as String;
  return menu;
}

String menuOfTea(String drinkCategory) {
  print('Menu of Tea :');
  print('1.Chrysanthemum Tea              20฿');
  print('2.Thai Milk Tea                  35฿');
  print('3.Taiwanese Tea                  35฿');
  print('4.Matcha Latte                   40฿');
  print('5.Limenade Tea                   20฿');
  print('6.Lychee Tea                     20฿');
  print('7.Strawberry Tea                 30฿');
  print('Please Select Menu of Tea(number): ');
  String menu = stdin.readLineSync() as String;
  return menu;
}

String menuOfCoffee(String drinkCategory) {
  print('Menu of Coffee :');
  print('1.Espresso                       25฿');
  print('2.Americano                      30฿');
  print('3.Latte                          35฿');
  print('4.Cappuccino                     35฿');
  print('5.Mocha                          40฿');
  print('6.Caramel Café Latte             40฿');
  print('7.Matcha Café Latte              45฿');
  print('8.Thai Tea Café Latte            40฿');
  print('9.Lychee Americano               30฿');
  print('Please Select Menu of Coffee(number): ');
  String menu = stdin.readLineSync() as String;
  return menu;
}

String selectDrinkCategory() {
  print('Drink Type:');
  print('1.Coffee');
  print('2.Tea');
  print('3.Milk, Choco & Caramel');
  print('4.Protein Shake');
  print('5.Soda & Other');
  print('Please Select Drink Category(number): ');
  String drinkCategory = stdin.readLineSync() as String;
  showSeparator();
  return drinkCategory;
}

void showSeparator() {
  print('--------------------------------');
}

void showWelcome() {
  print('Welcome to Tao-bin Beverage');
}
